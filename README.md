# Tarif list responsive

For implementing the solution I used Angular 8.2, Material 8.2, Angular-Flex, Angular CLI 8.3.

Since I'm working in full stack now (.net core + Angular), I mostly familiar and have rich experience with .net unit tests.

But I don't see a problem to examine Karma, Jasmin, Protractor etc. to write tests for UI part as well (just need some time).

Please, restore packages with `npm i`.

Ang run solution with `ng serve`. 

Navigate to `http://localhost:4200/`.
