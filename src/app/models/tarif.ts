import { TarifsParameter } from './tarifs-parameter';

export interface Tarif {
    name: string;
    parameters: TarifsParameter[];
}
