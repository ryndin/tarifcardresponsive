import { TarifParametersType } from '../enums/tarifs-parameter-type';

export interface TarifsParameter {
    type: TarifParametersType;
    value: string | number;
}
