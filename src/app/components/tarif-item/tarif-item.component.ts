import { Component, OnInit, Input } from '@angular/core';
import { Tarif } from '../../models/tarif';
import { TarifParametersType } from '../../enums/tarifs-parameter-type';
import { TarifsParameter } from '../../models/tarifs-parameter';

@Component({
  selector: 'app-tarif-item',
  templateUrl: './tarif-item.component.html',
  styleUrls: ['./tarif-item.component.less']
})
export class TarifItemComponent implements OnInit {

  @Input() tarif: Tarif;
  @Input() index: number;

  price: number | string;
  downloadSpeed: number | string;
  uploadSpeed: number | string;
  restParameters: TarifsParameter[];

  constructor() { }

  ngOnInit() {
    this.price = this.tarif.parameters.find(t => t.type === TarifParametersType.Price).value;
    this.downloadSpeed = this.tarif.parameters.find(t => t.type === TarifParametersType.Download).value;
    this.uploadSpeed = this.tarif.parameters.find(t => t.type === TarifParametersType.Upload).value;
    this.restParameters = this.tarif.parameters
      .filter(t => t.type !== TarifParametersType.Upload
        && t.type !== TarifParametersType.Download
        && t.type !== TarifParametersType.Price);
  }
}
