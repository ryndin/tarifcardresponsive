import { Component, OnInit } from '@angular/core';
import { TarifService } from '../../services/tarif/tarif.service';
import { Tarif } from '../../models/tarif';
import { TarifParametersType } from '../../enums/tarifs-parameter-type';
import { SortDirection } from '../../enums/sort-direction';

@Component({
  selector: 'app-tarifs-table',
  templateUrl: './tarifs-table.component.html',
  styleUrls: ['./tarifs-table.component.less']
})
export class TarifsTableComponent implements OnInit {

  tarifs: Tarif[];
  SortDirection = SortDirection;
  sortTypes;

  constructor(private tarifService: TarifService) { }

  ngOnInit() {
    this.sortTypes = [];
    const sortKeys = Object.keys(TarifParametersType).filter(key => !isNaN(Number(TarifParametersType[key])));
    sortKeys.forEach(element => {
      this.sortTypes.push({direction: SortDirection.Asc, key: element});
      this.sortTypes.push({direction: SortDirection.Desc, key: element});
    });

    this.tarifService
      .getTarifs()
      .subscribe(t => this.tarifs = t);
  }

  sortChanged(sortOrder): void {
    this.tarifService
      .getOrderedTarifs(sortOrder.key, sortOrder.direction)
      .subscribe(t => this.tarifs = t);
  }
}
