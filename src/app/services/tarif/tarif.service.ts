import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { Tarif } from '../../models/tarif';
import { TarifParametersType } from '../../enums/tarifs-parameter-type';
import { TarifsParameter } from '../../models/tarifs-parameter';
import { tap } from 'rxjs/operators';
import { SortDirection } from '../../enums/sort-direction';

@Injectable({
  providedIn: 'root'
})
export class TarifService {

  constructor() { }

  getOrderedTarifs(sortName: string, sortDirection: SortDirection): Observable<Tarif[]> {

    const tarifs: Observable<Tarif[]> = this.getValues();

    const sorted = tarifs.pipe(
      tap(results => {
        return results.sort((a, b) => {

          let paramA;
          let paramB;

          if (sortDirection === SortDirection.Asc) {
            paramA = a.parameters.find(p => p.type === TarifParametersType[sortName]).value;
            paramB = b.parameters.find(p => p.type === TarifParametersType[sortName]).value;
          } else {
            paramB = a.parameters.find(p => p.type === TarifParametersType[sortName]).value;
            paramA = b.parameters.find(p => p.type === TarifParametersType[sortName]).value;
          }

          if (paramA > paramB) {
            return 1;
          } else if (paramA < paramB) {
            return -1;
          } else {
            return 0;
          }

        });
      }));

    return sorted;
  }

  getTarifs(): Observable<Tarif[]> {
    return this.getValues();
  }

  private getValues(): Observable<Tarif[]> {

    const tarif1: Tarif = {
      name: 'first tarif',
      parameters: [
        { type: TarifParametersType.Price, value: 120 },
        { type: TarifParametersType.Download, value: 12 },
        { type: TarifParametersType.Upload, value: 50 },
        { type: TarifParametersType.Benefit, value: 'discount' },
        { type: TarifParametersType.Benefit, value: 'free of charge' }
      ]
    };

    const tarif2: Tarif = {
      name: 'second tarif',
      parameters: [
        { type: TarifParametersType.Price, value: 230 },
        { type: TarifParametersType.Download, value: 100 },
        { type: TarifParametersType.Upload, value: 200 },
        { type: TarifParametersType.Benefit, value: 'discount' },
        { type: TarifParametersType.Benefit, value: 'quick termination' }
      ]
    };

    const tarif3: Tarif = {
      name: 'third tarif',
      parameters: [
        { type: TarifParametersType.Price, value: 80 },
        { type: TarifParametersType.Download, value: 40 },
        { type: TarifParametersType.Upload, value: 50 },
        { type: TarifParametersType.Benefit, value: 'low price' }
      ]
    };

    const tarifs: Tarif[] = [tarif1, tarif2, tarif3];

    return from([tarifs]);
  }
}

